# **Desafio React Native**

---

Olá, bem vindo(a) à **Paytime Fintech**!

Primeiramente, agradecemos pelo seu interesse em trabalhar conosco.

Este teste servirá para que a gente conheça um pouco mais sobre suas habilidades
técnicas no desenvolvimento de aplicativos com **React Native**.
Alguns dias após você finalizar o desafio (no máximo, uma semana), enviaremos
um feedback com nossas impressões e um possível convite para uma conversa mais
detalhada sobre as decisões que você tomou durante o desenvolvimento.

## **issue-time - App de acompanhamento de issues de repositórios**

---

Gostaríamos de um aplicativco simples para acampanhar _issues_ de repositórios
de organizações que estejam públicos no GitHub. O app deve permitir salvar um
repositório, ver as _issues_ do repositório salvo, filtrar _issues_ pelo
status (todas, abertas, fechadas) e abrir um _issue_ no navegador instalado no
dispositivo.

## **Orientações**

---

Nos mostre um aplicativo que seja simples de manter e se comporte bem em
diferentes dispositivos. Poderemos testar seu aplicativo em dispositivos reais
ou emuladores, que utilizando Android ou iOS (**não** é necessário suporte a
iPads ou smart watches).

O aplicativo deve seguir de maneira fiel o layout disponibilizado, performar
bem (não travar durante o uso ou em transições de tela) e não consumir gigabytes
de armazenamento no dispositivo.

Utilize qualquer módulo de terceiros que achar apropriado, mas esteja preparado
para justificar suas escolhas.

Faça tratamento de erros para requisições da API, exibindo a mensagem de erro
quando algum problema ocorrer. Fique livre para escolher a maneira de exibir
as mensagens de erro.

## **Requisitos e Regras**

---

- Utilizar React Native e TypeScript;
- Funcionar em dispositivos Android e iOS;
- Utilizar a React Native CLI (não fazer com Expo);
- Utilizar [`react-navigation`](https://reactnavigation.org/) para fazer a
  navegação entre as telas;
- A lista de repositórios deve ser persistente, ou seja, caso o app seja
  encerrado, ao abrí-lo novamente, a lista de **repositórios** deverá ser apresentada
  como estava antes do app ser encerrado.
- O usuário poderá atualizar a lista de repositórios e _issues_ arrastando a lista
  para baixo;
- Na listagem de _issues_, o título da _issue_ deve ocupar no máximo 1 linha. Caso
  o conteúdo seja maior que 1 linha, exibir "...", no final do texto para indicar que o
  mesmo possui mais conteúdo;

## **issue.time - Fluxo de telas**

---

A tela inicial deverá apresentar um campo de texto (onde será possível adicionar
repositórios) e a lista de repositórios adicionados previamente. Caso nenhum
repositório tenha sido adicionado, deverá exibir a mensagem de que a lista está vazia;

Ao pressionar um repositório adicionado, o app deve navegar para a tela
de _issues_, onde irá listar as _issues_ do repositório selecionado. Nesta tela,
deve ser possível filtrar entre todas as _issues_, somente _issues_ abertas ou
somente _issues_ fechadas. Caso nenhuma issue seja encontrada, exibir a mensagem
de lista vazia.

Ao pressionar uma issue, o app deve abrir a página HTML dessa _issue_ no
navegador do dispositivo. Não é necessário uma tela no aplicativo para visualizar
a _issue_.

## **issue.time - Layout**

---

Você deverá seguir o layout do link: [issue-time - Figma](https://www.figma.com/file/S7crGCWjmlKwfLxVp5RNzN/Paytime---Desafio-Mobile?node-id=18838%3A70511)

## **Como submeter?**

---

Você deve criar um _fork_ deste repositório desafio. Ao terminar o desafio,
você devera abrir uma **Pull Request** do seu _fork_ para este repositório.

Commite suas alterações de forma organizada, de maneira que seja possível entender
sua linha de raciocínio através dos _commmits_. Utilize mensagens de _commit_
sucintas mas que expliquem de maneira satisfatória o que foi desenvolvido.

Após abrir a **Pull Request**, envie um e-mail para `reginaldo.gutter@paytime.com.br`
com o seguinte conteúdo:

- Assunto: Quero ser Paytimer!
- Corpo:
    - [explicar qualquer ponto que você acredita ser relevante]
    - Link da **Pull Request**

Nós iremos baixar, executar, avaliar e retornar seu e-mail com um _feedback_ em até uma semana.

## **Enpoints e documentação**

---

- Recuperar um repositório: `GET https://api.github.com/repos/facebook/react-native`
- Recuperar _issues_: `GET https://api.github.com/repos/facebook/react-native/issues`
- Filtrar _issues_:
    - Todas: `GET https://api.github.com/repos/facebook/react-native/issues?state=all`
    - Abertas: `GET https://api.github.com/repos/facebook/react-native/issues?state=open`
    - Fechadas: `GET https://api.github.com/repos/facebook/react-native/issues?state=closed`

## **Desejável**

---

- Organização do código;
- Código componentizado e simples de manter;
- Utilizar Clean Code;
- TypeScript;
- Descrição da PR em inglês;

## **Diferenciais**

---

- Fazer testes unitários e/ou end-to-end;
